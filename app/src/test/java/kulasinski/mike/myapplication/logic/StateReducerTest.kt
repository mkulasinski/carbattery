package kulasinski.mike.myapplication.logic

import kulasinski.mike.myapplication.logic.run.ElectricOn
import kulasinski.mike.myapplication.logic.run.State
import kulasinski.mike.myapplication.logic.run.StateReducer
import org.junit.Assert.*
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class StateReducerTest {
    @Test
    fun carAudioTurnsOn() {
        val state = State(
            parts = listOf(
                Part.CarAudio(
                    10,
                    1
                )
            )
        )
        val reducer = StateReducer()

        val stateOut = reducer(state, ElectricOn(10))

        assertNotNull(stateOut.partsOn.firstOrNull { it.id == 10L})
    }
//
//    @Test
//    fun allLightsOn() {
//        val state = State(
//            partsOff = listOf(
//                HighBeamLight(false, 1),
//                LowBeamLight(false, 1),
//                ParkingLight(false, 1)
//            )
//        )
//        val reducer = StateReducer()
//
//        val stateOut = reducer(
//            state,
//            LightsOn(LightsType.ALL)
//        )
//
//        stateOut
//            .partsOff
//            .filterIsInstance<Light>()
//            .forEach { assertTrue(it.isOn) }
//    }
//
//    @Test
//    fun notEnoughEnergy() {
//        val state = State(
//            partsOff = listOf(
//                Audio(false, 1),
//                HighBeamLight(true, 1),
//                LowBeamLight(false, 1),
//                ParkingLight(true, 1)
//            )
//        )
//        val reducer = StateReducer()
//
//        val stateOut = reducer(state, EnergyNotEnough)
//
//        stateOut
//            .partsOff
//            .filterIsInstance<EnergyConsumer>()
//            .forEach { assertFalse(it.isOn) }
//
//        assertTrue(stateOut.notEnoughEnergy)
//    }
}
