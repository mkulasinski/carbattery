package kulasinski.mike.myapplication.logic.run

sealed class Effect
object EngineStarted : Effect()
object EngineStopped : Effect()
object IgnitionOn : Effect()
object IgnitionOff : Effect()
data class ElectricOn(val id : Long) : Effect()
data class ElectricOff(val id : Long) : Effect()
data class EnergyInfo(val availableEnergy: Int, val energyUsage: Int) : Effect()
object EnergyNotEnough : Effect()