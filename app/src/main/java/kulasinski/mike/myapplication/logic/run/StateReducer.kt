package kulasinski.mike.myapplication.logic.run

import kulasinski.mike.myapplication.logic.Part

class StateReducer : (State, Effect) -> State {
    override fun invoke(state: State, effect: Effect): State {
        return when (effect) {
            is EngineStarted -> state.copy(isEngineRunning = true)
            is EngineStopped -> state.copy(isEngineRunning = false)
            is IgnitionOn -> state.copy(isIgnitionOn = true)
            is IgnitionOff -> state.copy(isIgnitionOn = false)
            is ElectricOn -> state.copy(partsOn = state.turnOn(effect.id))
            is ElectricOff -> state.copy(partsOn = state.turnOff(effect.id))
            is EnergyInfo -> state.copy(
                availableEnergy = effect.availableEnergy,
                energyUsage = effect.energyUsage,
                notEnoughEnergy = false
            )
            is EnergyNotEnough -> state.copy(
                availableEnergy = 0,
                energyUsage = 0,
                notEnoughEnergy = true,
                partsOn = emptyList()
            )
        }
    }

    private fun State.turnOn(id: Long): List<Part> {
        return if (partsOn.any { it.id == id }) partsOn
        else partsOn + parts.filter { it.id == id }
    }

    private fun State.turnOff(id: Long): List<Part> {
        return partsOn - partsOn.filter { it.id == id }
    }
}