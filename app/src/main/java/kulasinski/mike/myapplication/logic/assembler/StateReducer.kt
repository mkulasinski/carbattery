package kulasinski.mike.myapplication.logic.assembler

import kulasinski.mike.myapplication.logic.common.StateReducer

class StateReducer : StateReducer<State, Effect> {
    override fun invoke(state: State, effect: Effect): State {
        return when (effect) {
            is Effect.AttachedParts -> state.copy(attachedParts = effect.parts)
            is Effect.PartsAvailable -> state.copy(availableParts = state
                .availableParts
                .toMutableMap()
                .apply {
                    remove(effect.partType)
                    put(effect.partType, effect.parts)
                }
            )
        }
    }
}