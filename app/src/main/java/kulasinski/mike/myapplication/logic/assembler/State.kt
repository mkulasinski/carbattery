package kulasinski.mike.myapplication.logic.assembler

import kulasinski.mike.myapplication.logic.Part
import kulasinski.mike.myapplication.ui.PartType

data class State(
    val attachedParts: List<Part> = emptyList(),
    val availableParts: Map<PartType, List<Part>> = emptyMap()
)