package kulasinski.mike.myapplication.logic.assembler

import kulasinski.mike.myapplication.logic.Part
import kulasinski.mike.myapplication.ui.PartType
import kulasinski.mike.myapplication.logic.Effect as Message

sealed class Effect : Message {
    data class AttachedParts(val parts : List<Part>) : Effect()
    data class PartsAvailable(val parts : List<Part>, val partType: PartType) : Effect()
}