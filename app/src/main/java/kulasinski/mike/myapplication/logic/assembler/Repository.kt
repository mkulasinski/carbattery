package kulasinski.mike.myapplication.logic.assembler

import io.reactivex.Observable
import kulasinski.mike.myapplication.logic.Part

interface Repository {
    fun getParts(): Observable<List<Part>>
}

/**
 * Could be easily DB
 */
object DummyRepository : Repository {

    private val parts: List<Part>
        get() {
            return listOf(
                Part.CarAudio(1, "Pioneer", 150),
                Part.CarAudio(2, "Boss", 250),
                Part.Battery(3, "Bosh", 1000),
                Part.Battery(4, "Bosh Premium", 1500),
                Part.Alternator(5, "Alternator Weak", 1500),
                Part.Alternator(6, "Alternator Strong", 1800),
                Part.ParkingLight(7, "Cheap Parking light", 2),
                Part.LowBeamLight(8, "Cheap Low light", 10),
                Part.HighBeamLight(9, "Cheap High light", 15)
            )
        }

    override fun getParts(): Observable<List<Part>> {
        return Observable.just(parts)
    }

}