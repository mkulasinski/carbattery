package kulasinski.mike.myapplication.logic.assembler

import kulasinski.mike.myapplication.ui.PartType
import kulasinski.mike.myapplication.logic.Event as Message

sealed class Event : Message {
    data class GetPartsList(val partType: PartType) : Event()
    data class AttachPart(val id: Long): Event()
    data class DetachPart(val id: Long): Event()
}