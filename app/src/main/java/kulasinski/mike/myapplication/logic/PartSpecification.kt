package kulasinski.mike.myapplication.logic

interface PartSpec

interface EnergyProducer : PartSpec {
    var chargeAmount: Int
}

interface EnergySource : PartSpec {
    var chargeAvailable: Int
}

interface EnergyConsumer : PartSpec {
    var useAmount: Int
}

interface Toggleable : PartSpec
interface RunWithoutIgnition : PartSpec