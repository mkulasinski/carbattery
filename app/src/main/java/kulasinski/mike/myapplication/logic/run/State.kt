package kulasinski.mike.myapplication.logic.run

import kulasinski.mike.myapplication.logic.Part

data class State(
    val parts: List<Part> = emptyList(),
    val partsOn: List<Part> = emptyList(),
    val isIgnitionOn: Boolean = false,
    val isEngineRunning: Boolean = false,
    val notEnoughEnergy: Boolean = false,
    val availableEnergy: Int = 0,
    val energyUsage: Int = 0
)