package kulasinski.mike.myapplication.logic.assembler

import io.reactivex.Observable
import kulasinski.mike.myapplication.logic.Part
import kulasinski.mike.myapplication.logic.common.Actor
import kulasinski.mike.myapplication.ui.PartType

class Actor(private val repository: Repository) : Actor<Event, State, Effect> {

    override fun invoke(event: Event, state: State): Observable<Effect> {
        return when (event) {
            is Event.GetPartsList -> {
                repository
                    .getParts()
                    .map { it.filter(event.partType) }
                    .map { Effect.PartsAvailable(it, event.partType) }
            }
            is Event.AttachPart -> {
                repository
                    .getParts()
                    .flatMapIterable { it -> it }
                    .filter { it.id == event.id }
                    .map { state.attachedParts - state.attachedParts.filterIsInstance(it::class.java) + it }
                    .map(Effect::AttachedParts)
            }
            is Event.DetachPart -> {
                repository
                    .getParts()
                    .flatMapIterable { it -> it }
                    .filter { it.id == event.id }
                    .map { state.attachedParts - it }
                    .map(Effect::AttachedParts)
            }
        }.cast(Effect::class.java)
    }

    private fun List<Part>.filter(partType: PartType): List<Part> {
        return filter {
            when (partType) {
                PartType.ALTERNATOR -> it is Part.Alternator
                PartType.BATTERY -> it is Part.Battery
                PartType.CAR_AUDIO -> it is Part.CarAudio
                PartType.LIGHT_PARKING -> it is Part.ParkingLight
                PartType.LIGHT_HIGH_BEAM -> it is Part.HighBeamLight
                PartType.LIGHT_LOW_BEAM -> it is Part.LowBeamLight
            }
        }
    }
}