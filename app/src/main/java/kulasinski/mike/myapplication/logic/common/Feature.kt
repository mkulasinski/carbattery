package kulasinski.mike.myapplication.logic.common

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

interface Actor<Event, State, Effect> : (Event, State) -> Observable<Effect>
interface StateReducer<State, Effect> : (State, Effect) -> State

open class Feature<State, Event, Effect>(
    initialState: State,
    events: Observable<Event>,
    actor: Actor<Event, State, Effect>,
    reducer: StateReducer<State, Effect>
) {
    private var lastState = initialState

    var stateStream = events
        .flatMap<Effect> { event -> actor(event, lastState) }
        .observeOn(AndroidSchedulers.mainThread())
        .scan(initialState, reducer)
        .doOnNext { lastState = it }
        .replay(1)
        .autoConnect(0)
}
