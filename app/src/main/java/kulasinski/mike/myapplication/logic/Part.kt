package kulasinski.mike.myapplication.logic

sealed class Part {
    abstract val id: Long
    abstract val name: String

    data class Battery(
        override val id: Long,
        override val name: String,
        override var chargeAvailable: Int
    ) : Part(), EnergySource

    data class Alternator(
        override val id: Long,
        override val name: String,
        override var chargeAmount: Int
    ) : Part(), EnergyProducer

    data class CarAudio(
        override val id: Long,
        override val name: String,
        override var useAmount: Int
    ) : Part(), EnergyConsumer, RunWithoutIgnition, Toggleable

    data class LowBeamLight(
        override val id: Long,
        override val name: String,
        override var useAmount: Int
    ) : Part(), EnergyConsumer, Toggleable

    data class HighBeamLight(
        override val id: Long,
        override val name: String,
        override var useAmount: Int
    ) : Part(), EnergyConsumer, Toggleable

    data class ParkingLight(
        override val id: Long,
        override val name: String,
        override var useAmount: Int
    ) : Part(), EnergyConsumer, RunWithoutIgnition, Toggleable

}

