package kulasinski.mike.myapplication.logic.assembler

import kulasinski.mike.myapplication.EventDispatcher
import kulasinski.mike.myapplication.logic.common.Feature

object AssemblerFeature : Feature<State, Event, Effect>(
    initialState = State(),
    actor = Actor(DummyRepository),
    events = EventDispatcher.ofType(Event::class.java),
    reducer = StateReducer()
)