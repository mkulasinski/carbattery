package kulasinski.mike.myapplication

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

object EventDispatcher {
    private val subject = BehaviorSubject.create<Any>()

    operator fun invoke(event: Any) {
        subject.onNext(event)
    }

    fun <T> ofType(klass: Class<T>): Observable<T> {
        return subject.ofType(klass)
    }
}