package kulasinski.mike.myapplication.ui.assembler

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kulasinski.mike.myapplication.EventDispatcher
import kulasinski.mike.myapplication.R
import kulasinski.mike.myapplication.logic.*
import kulasinski.mike.myapplication.logic.assembler.AssemblerFeature
import kulasinski.mike.myapplication.logic.assembler.Event.AttachPart
import kulasinski.mike.myapplication.logic.assembler.Event.GetPartsList
import kulasinski.mike.myapplication.ui.PartType

class PartList : AppCompatActivity() {

    var boundary = PartListBoundary(AssemblerFeature)

    val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_part_list)

        val adapter = Adapter()
        val recyclerView = findViewById<RecyclerView>(R.id.partList_recyclerView)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        boundary
            .updates(IntentCreator.getPartType(intent))
            .subscribe(adapter::update)
            .also { disposable.add(it) }

        EventDispatcher
            .ofType(UIEvent.Choose::class.java)
            .subscribe { finish() }
            .also { disposable.add(it) }
    }

    override fun onResume() {
        super.onResume()
        EventDispatcher(GetPartsList(IntentCreator.getPartType(intent)))
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    object IntentCreator : (Context, PartType) -> Intent {
        private const val PART_TYPE = "PART_TYPE"

        override fun invoke(context: Context, partType: PartType): Intent {
            return Intent(context, PartList::class.java).putExtra(PART_TYPE, partType)
        }

        fun getPartType(intent: Intent): PartType {
            return intent.getSerializableExtra(PART_TYPE) as PartType
        }
    }


    sealed class UIEvent : Event {
        data class Choose(val id: Long) : UIEvent()
    }


    data class ViewModel(
        val partId: Long,
        val partName: String,
        val partType: PartType,
        val partInfo: String
    )

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name = view.findViewById<TextView>(R.id.partList_name)
        private val type = view.findViewById<TextView>(R.id.partList_type)
        private val info = view.findViewById<TextView>(R.id.partList_info1)

        fun bind(model: ViewModel) {
            name.text = model.partName
            type.text = model.partType.name
            info.text = model.partInfo
            itemView.setOnClickListener {
                EventDispatcher(AttachPart(model.partId))
                EventDispatcher(UIEvent.Choose(model.partId))
            }
        }
    }

    class Adapter : RecyclerView.Adapter<ViewHolder>() {

        private var models = emptyList<ViewModel>()

        init {
            setHasStableIds(true)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.activity_part_list_item,
                    parent,
                    false
                )
            )
        }

        fun update(models: List<ViewModel>) {
            this.models = models
            notifyDataSetChanged()
        }

        override fun getItemId(position: Int): Long = models[position].partId

        override fun getItemCount(): Int = models.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(models[position])
        }

    }

    class PartListBoundary(
        private val feature: AssemblerFeature
    ) {
        fun updates(partType: PartType): Observable<List<ViewModel>> {
            return feature
                .stateStream
                .map { it.availableParts.asViewModel(partType) }
        }


        private fun Map<PartType, List<Part>>.asViewModel(partType: PartType): List<ViewModel> {
            return get(partType)?.let {
                it.map { item ->
                    when (item) {
                        is EnergySource -> ViewModel(item.id, item.name, partType, item.chargeAvailable.toString())
                        is EnergyProducer -> ViewModel(item.id, item.name, partType, item.chargeAmount.toString())
                        is EnergyConsumer -> ViewModel(item.id, item.name, partType, item.useAmount.toString())
                        else -> throw RuntimeException()
                    }
                }
            } ?: emptyList()
        }
    }
}