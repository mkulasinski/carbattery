package kulasinski.mike.myapplication.ui.assembler

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kulasinski.mike.myapplication.EventDispatcher
import kulasinski.mike.myapplication.R
import kulasinski.mike.myapplication.logic.Event
import kulasinski.mike.myapplication.logic.Part
import kulasinski.mike.myapplication.logic.assembler.AssemblerFeature
import kulasinski.mike.myapplication.ui.CarRunner
import kulasinski.mike.myapplication.ui.PartType
import kulasinski.mike.myapplication.ui.PartType.*

class CarAssembler : AppCompatActivity() {

    private val disposables = CompositeDisposable()
    private val boundary = Boundary(AssemblerFeature)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_assembler)

        EventDispatcher
            .ofType(UIEvent::class.java)
            .subscribe {
                when (it) {
                    is UIEvent.Choose -> startActivity(PartList.IntentCreator(this, it.partType))
                    is UIEvent.Next -> startActivity(Intent(this, CarRunner::class.java))
                }
            }
            .also { disposables.add(it) }

        val adapter = Adapter()
        val recyclerView = findViewById<RecyclerView>(R.id.assembler_recyclerView)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


        boundary
            .updates()
            .subscribe(adapter::update)
            .also { disposables.add(it) }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    sealed class UIEvent : Event {
        data class Choose(val partType: PartType) : UIEvent()
        object Next : UIEvent()
    }


    data class ViewModel(
        val partId: Long,
        val label: String,
        val partName: String,
        val type: PartType
    )

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val label = view.findViewById<TextView>(R.id.assembler_part_label)
        private val name = view.findViewById<TextView>(R.id.assembler_part_name)
        private val chooser = view.findViewById<TextView>(R.id.assembler_part_chooser)

        fun bind(model: ViewModel) {
            label.text = model.label
            name.text = model.partName
            chooser.setOnClickListener { EventDispatcher(UIEvent.Choose(model.type)) }
        }
    }

    class Adapter : RecyclerView.Adapter<ViewHolder>() {
        private var models = emptyList<ViewModel>()

        fun update(models: List<ViewModel>) {
            this.models = models
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.activity_car_assembler_part,
                    parent,
                    false
                )
            )
        }

        override fun getItemCount(): Int = models.size

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(models[position])
        }
    }

    class Boundary(private val feature: AssemblerFeature) {

        fun updates(): Observable<List<ViewModel>> {
            return feature
                .stateStream
                .map { it.attachedParts.asViewModel() }
        }

        private fun List<Part>.asViewModel(): List<ViewModel> = listOf(
            ViewModel(getId(BATTERY), "Battery", getName(BATTERY), BATTERY),
            ViewModel(getId(ALTERNATOR), "Alternator", getName(ALTERNATOR), ALTERNATOR),
            ViewModel(getId(CAR_AUDIO), "Car audio", getName(CAR_AUDIO), CAR_AUDIO),
            ViewModel(getId(LIGHT_PARKING), "Parking light", getName(LIGHT_PARKING), LIGHT_PARKING),
            ViewModel(getId(LIGHT_LOW_BEAM), "Low beam", getName(LIGHT_LOW_BEAM), LIGHT_LOW_BEAM),
            ViewModel(getId(LIGHT_HIGH_BEAM), "High beam", getName(LIGHT_HIGH_BEAM), LIGHT_HIGH_BEAM)
        )

        private fun List<Part>.getId(partType: PartType): Long {
            val clazz: Class<out Part> = when (partType) {
                ALTERNATOR -> Part.Alternator::class.java
                BATTERY -> Part.Battery::class.java
                CAR_AUDIO -> Part.CarAudio::class.java
                LIGHT_PARKING -> Part.ParkingLight::class.java
                LIGHT_HIGH_BEAM -> Part.HighBeamLight::class.java
                LIGHT_LOW_BEAM -> Part.LowBeamLight::class.java
            }
            return filterIsInstance(clazz).firstOrNull()?.id ?: -1
        }

        private fun List<Part>.getName(partType: PartType): String {
            val clazz: Class<out Part> = when (partType) {
                ALTERNATOR -> Part.Alternator::class.java
                BATTERY -> Part.Battery::class.java
                CAR_AUDIO -> Part.CarAudio::class.java
                LIGHT_PARKING -> Part.ParkingLight::class.java
                LIGHT_HIGH_BEAM -> Part.HighBeamLight::class.java
                LIGHT_LOW_BEAM -> Part.LowBeamLight::class.java
            }
            return filterIsInstance(clazz).firstOrNull()?.name ?: ""
        }
    }
}