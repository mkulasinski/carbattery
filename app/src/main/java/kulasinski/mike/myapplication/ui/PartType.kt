package kulasinski.mike.myapplication.ui

enum class PartType {
    ALTERNATOR,
    BATTERY,
    CAR_AUDIO,
    LIGHT_PARKING,
    LIGHT_HIGH_BEAM,
    LIGHT_LOW_BEAM
}