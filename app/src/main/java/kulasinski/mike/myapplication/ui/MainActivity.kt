package kulasinski.mike.myapplication.ui

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import kulasinski.mike.myapplication.R
import kulasinski.mike.myapplication.ui.assembler.CarAssembler

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<FloatingActionButton>(R.id.mainActivity_next).setOnClickListener {
            startActivity(
                Intent(
                    this,
                    CarAssembler::class.java
                )
            )
        }
    }
}
